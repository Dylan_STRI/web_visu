const express = require("express");
const path = require("path");
//Création du serveur express
const app = express();
const Influx = require("influx");
//const Influx = require("influxdb-nodejs");

// Connexion base de données
const influx = new Influx.InfluxDB({
    host: '172.16.96.154',
    database: 'projetindus',
    schema: [
      {
        measurement: 'image',
        fields: {
            time: Influx.FieldType.STRING,
            chemin: Influx.FieldType.STRING,
            value: Influx.FieldType.INTEGER,
        },
        tags: ['time', 'chemin', 'value']
      }
    ]
});
//const client = new Influx('http://172.16.96.154:8086/projetindus');

var chemin = "";
var nb_personnes = 0;

influx.query(`
    select * from image
  `).then(results => {
    chemin = results[0].chemin;
    nb_personnes = results[0].value;
  });
//client.query('image').then(console.info);
//const influx = new Influx.InfluxDB('http://2001:678:3fc:d6:b8ad:caff:fefe:6/64:8086/projetindus')

//Démarrage du serveur
app.listen(3000, () => {
  console.log("Serveur démarré (http://localhost:3000/) !");
});

// GET
app.get("/", (req, res) => {
    // res.send("Bonjour le monde...");
    res.render("index",{nombre_personne : nb_personnes, chemin_image : chemin});
  });

  // GET / about
app.get("/about", (req, res) => {
   res.render("about");
  });

// GET / data
app.get("/data", (req, res) => {
    const test = {
      titre: "Test",
      items: ["un", "deux", "trois"]
    };
    res.render("data", { model: test });
  });


// Configuration du serveur
app.set("view engine", "ejs");
app.set("views", __dirname + "/views");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, "public")));
